package main;

public class Visor {
	Rolo rolo;
	
	public Visor(Rolo rolo) {
		this.rolo = rolo;
	}
	
	public void imprimir(int sorteios[], int pontos) {
		for(int i = 0; i < sorteios.length; i++) {
			int sorteio = sorteios[i];
			String valor = rolo.valores[sorteio];
			
			System.out.print(String.format(" %s |", valor));
		}
		System.out.println("");
		System.out.println(String.format("Sua pontuação é %d.", pontos));
	}
}
