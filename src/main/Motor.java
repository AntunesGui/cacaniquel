package main;

import java.util.Random;

public class Motor {
	private Random random = new Random();
	private Rolo rolo;
	
	public Motor(Rolo rolo){
		this.rolo = rolo;
	}
	
	public int sortear() {
		return random.nextInt(rolo.valores.length);
	}
}
