package main;

public class App {
	public static void main(String[] args) {
		Rolo rolo = new Rolo();
		Motor motor = new Motor(rolo);
		Visor visor = new Visor(rolo);
		Calculador calculador = new Calculador(rolo);
		
		int[] sorteios = new int[3];
		
		for(int i = 0; i < 3; i++) {
			sorteios[i] = motor.sortear();
		}
		
		int pontos = calculador.calcular(sorteios);
		
		visor.imprimir(sorteios, pontos);
		
//		System.out.println(motor.sortear());
	}
}
